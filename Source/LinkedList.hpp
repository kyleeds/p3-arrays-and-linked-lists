//
//  LinkedList.hpp
//  CommandLineTool
//
//  Created by Kyle Edwards on 13/10/2016.
//  Copyright © 2016 Tom Mitchell. All rights reserved.
//

#ifndef LinkedList_hpp
#define LinkedList_hpp

#include <stdio.h>

class LinkedList
{
public:
    /**Constructor that initialises the head to point to nullptr*/
    LinkedList();
    
    /**Detructor that goes through and deletes all nodes in the list*/
    ~LinkedList();
    
    /**Adds new items to the end of the LinkedList*/
    void add(float intemValue);
    
    /**Returns the value stored in the node at the specified index*/
    float get (int index);
    
    /**returns the number of items currently in the LinkedList*/
    int size ();
    
private:
    struct Node
    {
        float value;
        Node* next;
    };
    Node* head;
};


#endif /* LinkedList_hpp */
