//
//  Array.cpp
//  CommandLineTool
//
//  Created by Kyle Edwards on 06/10/2016.
//  Copyright © 2016 Tom Mitchell. All rights reserved.
//

#include "Array.hpp"

Array::Array()
{
    numOfArray = 0;
    floatPoint = nullptr;
};
Array::~Array()
{
    delete floatPoint;
};
void Array::add (float itemValue)
{
    float* tempFloatPoint = new float[numOfArray+1];
    
    for (int index = 0; index < numOfArray; index += 1)
    {
        tempFloatPoint[index] = floatPoint[index];
    }
    
    delete[] floatPoint;
    
    floatPoint = tempFloatPoint;
    floatPoint[numOfArray] = itemValue;
    numOfArray += 1;
    
};
float Array::get (int index)
{
    return floatPoint[index-1];
};
int Array::size ()
{
    return numOfArray;
};
