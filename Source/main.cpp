//
//  main.cpp
//  CommandLineTool
//  Software Development for Audio
//

#include <iostream>
#include "LinkedList.hpp"
#include "Array.hpp"

int main (int argc, const char* argv[])
{
    LinkedList linkedList;
    float newValue;
    int listIndex;
    while (true)
    {
        std::cout << "Add new value";
        std::cin >> newValue;
        linkedList.add(newValue);
        linkedList.add(newValue + 1);
        
        std::cout << "Specify list index of the desired value.";
        std::cin >> listIndex;
        std::cout << "Value at the specified node is:" << linkedList.get(listIndex) << std::endl;
        
        std::cout << "Linked List Size:" << linkedList.size() << std::endl;
        
        std::cout << "All List Values:";
        for (int index = 1; index <= linkedList.size(); index += 1)
        {
            std::cout << linkedList.get(index) <<", ";
        }
        std::cout << std::endl;
        
        //        if (testArray() == true)
        //        {
        //            std::cout << "all Array tests passed\n";
        //        }
        //        else
        //        {
        //            std::cout << "Array tests failed\n";
        //        }
    }
    return 0;
}

